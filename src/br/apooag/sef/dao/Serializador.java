/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.apooag.sef.dao;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe responsável por serializar e deserializar objetos em arquivo.
 *
 * @author André & Gilmar
 */
public class Serializador {

    private static final Serializador conexao = new Serializador();

    /**
     * Construtor da classe
     */
    private Serializador() {

    }

    /**
     *
     * @return retorna uma instancia da classe Serializador
     */
    public static Serializador getSerializador() {
        return conexao;
    }

    /**
     * Serializa no arquivo a lista de objetos recebida como parâmetro.
     *
     * @param lista Lista de objetos
     * @param caminho Caminho do banco de dados.
     * @throws java.io.IOException
     */
    public void serializar(List<Object> lista, String caminho) throws IOException {
        FileOutputStream arquivo = new FileOutputStream(caminho);
        try (ObjectOutputStream stream = new ObjectOutputStream(arquivo)) {
            stream.writeObject(lista);
        }
    }

    /**
     * Deserializa a lista de objetos.
     *
     * @param caminho Caminho do banco de dados
     * @return Obejeto com as imagens anotadas.
     * @throws java.io.IOException
     * @throws java.lang.ClassNotFoundException
     */
    public Object deserializar(String caminho) throws IOException, ClassNotFoundException {
        try {
            FileInputStream arquivo = new FileInputStream(caminho);
            ObjectInputStream stream;
            stream = new ObjectInputStream(arquivo);
            Object objeto = stream.readObject();
            return objeto;
        } catch (FileNotFoundException ex) {
            System.out.println("Banco de dados vazio: " + ex.getMessage());
            return new ArrayList();
        }
    }
}
