/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.apooag.sef.dao;

import br.apooag.sef.entidade.Partida;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Gilmar
 */
public class PartidaDao {
    private final String caminho;
    
    public PartidaDao(String caminho){
        this.caminho = caminho;
    }
    
    public ArrayList<Partida> inicializar() {
        try {
            return (ArrayList<Partida>) Serializador.getSerializador().deserializar(caminho);
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage() );
            return new ArrayList();
        }  
    }
    
    public void salvar(ArrayList<Partida> historico) {
        List<Object> objetos = new ArrayList();
        if(!historico.isEmpty()){
            for(Partida partida : historico){
               objetos.add( (Object) partida);
            }
        }
        
        try {
            Serializador.getSerializador().serializar(objetos, this.caminho);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
