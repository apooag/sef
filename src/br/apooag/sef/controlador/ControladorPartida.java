/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.apooag.sef.controlador;

import br.apooag.sef.entidade.EstadoPartida;
import br.apooag.sef.entidade.Intensidade;
import br.apooag.sef.entidade.Partida;
import br.apooag.sef.fronteira.Eyetracker;
import br.apooag.sef.fronteira.Sensor;
import br.apooag.sef.fronteira.TelaPartida;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Gilmar
 */
public final class ControladorPartida {

    private static final TelaPartida telaPartida = new TelaPartida();
    private static Partida partidaAtual;
    private static final Eyetracker eyetracker = new Sensor();
    private static Timer timer;
    private static int contadorAguardando;
    private static int contadorRealizandoAcao;
    private static int contadorRealizandoAcaoInusitada;
    private static int contadorChamandoAtencao;

    private ControladorPartida() {
        
    }

    public static void iniciarPartida() {
        partidaAtual = new Partida(ControladorJogo.getPersonagemAtual(), ControladorJogo.getTipoAcaoAtual());
        eyetracker.ativar(partidaAtual.getPersonagem().getPosicaoOlhos(), partidaAtual.getPersonagem().getDiametroOlhos());
        String caminho = "../recursos/" +partidaAtual.getPersonagem().name()+"/";
        telaPartida.setCaminho(caminho);
        telaPartida.setVisible(true);        
        telaPartida.desenhar(ControladorComportamento.getAguardando().getRecurso(), partidaAtual.getEstado(), partidaAtual.getNivelHabilidade(), 0);

        resetarContadores();
        timer = new Timer();
        timer.schedule(new ControladorPartida.Loop(), 0, 1 * 1000);
    }

    public static void interacaoDetectada(int duracao) {
        if (duracao > 0) {
            Intensidade intensidade = partidaAtual.addInteracao(duracao);
            telaPartida.desenhar(ControladorComportamento.getAcao(intensidade).getRecurso(), partidaAtual.getEstado(), partidaAtual.getNivelHabilidade(), duracao);
            resetarContadores();
            
            if(partidaAtual.isGanhou()){
                partidaAtual.setEstado(EstadoPartida.REALIZANDO_ACAO_INUSITADA);
                telaPartida.desenhar(ControladorComportamento.getAcaoInusitada().getRecurso(), partidaAtual.getEstado(), partidaAtual.getNivelHabilidade(), duracao);
                System.out.println("GANHOU!");
                partidaAtual.setGanhou(false);
            }
        }

    }

    private static void resetarContadores() {
        contadorAguardando = 0;
        contadorChamandoAtencao = 0;
        contadorRealizandoAcao = 0;
        contadorRealizandoAcaoInusitada = 0;
    }

    private static class Loop extends TimerTask {

        @Override
        public void run() {
            EstadoPartida estadoAtual = partidaAtual.getEstado();

            if (estadoAtual.equals(EstadoPartida.AGUARDANDO)) {
                contadorAguardando++;
                if (contadorAguardando == 30) {
                    partidaAtual.setEstado(EstadoPartida.CHAMANDO_ATENCAO);
                    resetarContadores();
                    telaPartida.desenhar(ControladorComportamento.getAlerta().getRecurso(), partidaAtual.getEstado(), partidaAtual.getNivelHabilidade(), 0);
                }
            } else if (estadoAtual.equals(EstadoPartida.CHAMANDO_ATENCAO)) {
                contadorChamandoAtencao++;
                if (contadorChamandoAtencao == 5) {
                    partidaAtual.setEstado(EstadoPartida.AGUARDANDO);
                    telaPartida.desenhar(ControladorComportamento.getAguardando().getRecurso(), partidaAtual.getEstado(), partidaAtual.getNivelHabilidade(), 0);
                    resetarContadores();
                }
            } else if (estadoAtual.equals(EstadoPartida.REALIZANDO_ACAO)) {
                contadorRealizandoAcao++;
                if (contadorRealizandoAcao == 5) {
                    partidaAtual.setEstado(EstadoPartida.AGUARDANDO);
                    telaPartida.desenhar(ControladorComportamento.getAguardando().getRecurso(), partidaAtual.getEstado(), partidaAtual.getNivelHabilidade(),0);
                    resetarContadores();
                }
            } else if (estadoAtual.equals(EstadoPartida.REALIZANDO_ACAO_INUSITADA)) {
                contadorRealizandoAcaoInusitada++;
                if (contadorRealizandoAcaoInusitada == 10) {
                    partidaAtual.setEstado(EstadoPartida.AGUARDANDO);
                    telaPartida.desenhar(ControladorComportamento.getAguardando().getRecurso(), partidaAtual.getEstado(), partidaAtual.getNivelHabilidade(),0);
                    resetarContadores();
                }
            }

        }
    }

    public static void finalizarPartida() {
        eyetracker.desativar();
        timer.cancel();
        resetarContadores();
        
        //ControladorHistorico.registrarPartida(partidaAtual);
        //ControladorHistorico.fecharHistorico();
        
    }
    
    public static void gravarPartida() {
    ControladorHistorico.registrarPartida(partidaAtual);
        ControladorHistorico.fecharHistorico();
    }

    public static Partida getPartida() {
        return partidaAtual;
    }

    public static void voltarParaInicio() {
        finalizarPartida();
        telaPartida.setVisible(false);
        ControladorJogo.exibirTelaInicial();
    }

}
