/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.apooag.sef.controlador;

import br.apooag.sef.entidade.Acao;
import br.apooag.sef.entidade.AcaoInusitada;
import br.apooag.sef.entidade.Aguardando;
import br.apooag.sef.entidade.Alerta;
import br.apooag.sef.entidade.Intensidade;
import br.apooag.sef.entidade.Realizavel;
import br.apooag.sef.entidade.TipoAcao;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author Gilmar
 */
public final class ControladorComportamento {
    private static Realizavel ultimaAcao;
    private static Realizavel ultimoAlerta;
    private static final Set<Realizavel> acoesDisponiveis = new HashSet<>();
    private static final Set<Realizavel> acoesInusitadasDisponiveis = new HashSet<>();
    private static final Set<Realizavel> alertasDisponiveis = new HashSet<>();
    private static final Set<Realizavel> aguardandoDisponiveis = new HashSet<>();
 
    private ControladorComportamento(){
        
    }
    
    public static void inicializar(){
        //ações
        acoesDisponiveis.add(new Acao("Bater Cabeça", new String[]{"REALIZANDO_ACAO", "REALIZANDO_ACAO2"}, TipoAcao.TRAGEDIA));
        
        //ações inusitadas
        acoesInusitadasDisponiveis.add(new AcaoInusitada("Ação Inusitada Tragédia", TipoAcao.TRAGEDIA, "REALIZANDO_ACAO_INUSITADA"));
        
        //alertas
        alertasDisponiveis.add(new Alerta("Piscada", "CHAMANDO_ATENCAO"));
        
        //aguardando
        aguardandoDisponiveis.add(new Aguardando("AGUARDANDO"));
    }
    
    public static Realizavel getAlerta(){
        Realizavel alerta = null;
        Iterator i = alertasDisponiveis.iterator();
        if(i.hasNext()){
             alerta = alertasDisponiveis.iterator().next();
             ultimoAlerta = alerta;
        }
        return alerta;
    }
    
    public static Realizavel getAguardando(){
        Realizavel aguardando = null;
        Iterator i = aguardandoDisponiveis.iterator();
        if(i.hasNext()){
             aguardando = aguardandoDisponiveis.iterator().next();
        }
        return aguardando;
    }
    
    public static Realizavel getAcao(Intensidade intensidade){
        Acao acao = null;
        Iterator i = acoesDisponiveis.iterator();
        if(i.hasNext()){
             acao = (Acao) acoesDisponiveis.iterator().next();
             acao.setIntensidade(intensidade);
             ultimaAcao = (Realizavel)acao;
        }
       return (Realizavel)acao;
    }
    
    public static Realizavel getAcaoInusitada(){
        Realizavel acaoInusitada = null;
        Iterator i = acoesInusitadasDisponiveis.iterator();
        if(i.hasNext()){
             acaoInusitada = acoesInusitadasDisponiveis.iterator().next();
        }
        return acaoInusitada;
    }
}
