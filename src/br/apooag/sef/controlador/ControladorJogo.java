/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.apooag.sef.controlador;

import br.apooag.sef.entidade.Personagem;
import br.apooag.sef.entidade.TipoAcao;
import br.apooag.sef.fronteira.*;

/**
 *
 * @author Gilmar
 */
public final class ControladorJogo {
    private static Personagem personagemAtual;
    private static TipoAcao tipoAcaoAtual;
    
    private static final TelaInicial telaInicial = new TelaInicial();
    private static final TelaEscolherPersonagem telaEscolherPersonagem = new TelaEscolherPersonagem();
    private static final TelaEscolherTipoAcao telaEscolherTipoAcao = new TelaEscolherTipoAcao();
    private static final TelaAcompEvoFX telaAcompanharEvolucao = new TelaAcompEvoFX();
    private static final TelaEscolherPersonagemEvolucao telaEscolherAcompanharEvolucao = new TelaEscolherPersonagemEvolucao();

   
    
    private ControladorJogo(){}
    
    public static void inicializar(){
        ControladorHistorico.abrirHistorico();
        ControladorComportamento.inicializar();
    }
    
    public static void finalizar(){
        ControladorHistorico.fecharHistorico();
    }
    
    public static void exibirTelaInicial(){
        telaInicial.setVisible(true);
    }
    public static void exibirTelaAcompanharEvolucao(){
        telaEscolherAcompanharEvolucao.setVisible(true);
        telaInicial.setVisible(false);
    }
    
     public static void exibirTelaEvolucao() {
        telaAcompanharEvolucao.setVisible(true);
        telaAcompanharEvolucao.inicializar();
        telaInicial.setVisible(false);
    }
    public static void exibirTelaEscolherPersonagem(){
        telaEscolherPersonagem.setVisible(true);
        telaInicial.setVisible(false);
    }
    public static void exibirTelaEscolherTipoAcao(){
        telaEscolherTipoAcao.setVisible(true);
        telaEscolherPersonagem.setVisible(false);
    }
    
    public static void escolherPersonagem(Personagem personagem){
        personagemAtual = personagem;
        exibirTelaEscolherTipoAcao();
        
        System.out.println(personagemAtual);
    } 
    
    public static void escolherTipoAcao(TipoAcao tipoAcao){
        tipoAcaoAtual = tipoAcao;
        telaEscolherTipoAcao.setVisible(false);
        
        System.out.println(tipoAcaoAtual);
        ControladorPartida.iniciarPartida();
    } 
    
    public static void voltarParaInicio(){
        telaInicial.setVisible(true);
        telaEscolherPersonagem.setVisible(false);
        telaEscolherTipoAcao.setVisible(false);
        telaAcompanharEvolucao.setVisible(false);
    } 
    
    public static Personagem getPersonagemAtual() {
        return personagemAtual;
    }

   
    
    public static TipoAcao getTipoAcaoAtual() {
        return tipoAcaoAtual;
    }
    
}
