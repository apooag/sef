/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.apooag.sef.controlador;

import br.apooag.sef.dao.PartidaDao;
import br.apooag.sef.entidade.Partida;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Gilmar
 */
public final class ControladorHistorico {

    private static ArrayList<Partida> historico;
    private static int graficoBarQntd[] = {0, 0, 0, 0};
    private static ArrayList graficoBar2;
    private static final PartidaDao partidaDao = new PartidaDao("persistencia/bancoDeDados");

    private ControladorHistorico() {

    }

    public static void abrirHistorico() {
        historico = partidaDao.inicializar();
        System.out.println("Partidas resgatadas do banco");
    }

    public static void registrarPartida(Partida partida) {
        historico.add(partida);
    }

    public static void fecharHistorico() {
        partidaDao.salvar(historico);
        System.out.println("Partida salva com sucesso");
    }

    public static int[] criarGrafico() {
        abrirHistorico();
        for (int i = 0; i < historico.size(); i++) {
            if (historico.get(i).getNivelHabilidade().name().equals("POUCO")) {
                graficoBarQntd[0] += 1;
            } else if (historico.get(i).getNivelHabilidade().name().equals("MEDIO")) {
                graficoBarQntd[1] += 1;
            } else if (historico.get(i).getNivelHabilidade().name().equals("BOM")) {
                graficoBarQntd[2] += 1;
            } else {
                graficoBarQntd[3] += 1;
            }
        }
        return graficoBarQntd;
    }

    private void exibirGrafico() {

    }

    private void calcMedia() {

    }

    public static List<Partida> getHistorico() {
        return historico;
    }
}
