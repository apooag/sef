/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.apooag.sef.fronteira;

/**
 *
 * @author Gilmar
 */
public interface Eyetracker {
    public void ativar(int[] posicaoOlhos, int diametroOlhos);
    public void desativar();
}
