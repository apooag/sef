package br.apooag.sef.fronteira;

import br.apooag.sef.controlador.ControladorHistorico;
import br.apooag.sef.controlador.ControladorJogo;
import java.awt.Button;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.layout.GridPane;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class TelaAcompEvoFX extends JFrame {

    JFXPanel fxPanel;

    public TelaAcompEvoFX() {
         
    }
    
    public void inicializar() {
        ControladorHistorico.abrirHistorico();
        initSwingComponents();
        initFxComponents();
    }

    public void carregarEstatistias() {
        ControladorHistorico.abrirHistorico();
    }

    private void calcularEstatisticas() {

    }

    private void initSwingComponents() {
        JPanel mainPanel2 = new JPanel();
        mainPanel2.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
       
        c.fill = GridBagConstraints.HORIZONTAL;
        fxPanel = new JFXPanel();
        Label personagem = new Label("Personagem : " + ControladorHistorico.getHistorico().get(0).getPersonagem().name());
        c.weightx = 0.5;
        c.fill = GridBagConstraints.PAGE_START;
        c.gridx = 2;
        c.gridy = 0;
        mainPanel2.add(personagem, c);
        Label partida = new Label("Lista de partidas");
        c.weightx = 0.5;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 0;
        mainPanel2.add(partida, c);
        Button btnCancelar = new Button("cancelar");
        c.weightx = 0.5;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 3;
        c.gridy = 0;
        mainPanel2.add(btnCancelar, c);
        c.weightx = 500;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.gridy = 1;
        mainPanel2.add(fxPanel, c);
        btnCancelar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ControladorJogo.voltarParaInicio();
            }
        });

        
        this.add(mainPanel2);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1200, 500);
    }

    private void initFxComponents() {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                GridPane grid = new GridPane();
                Scene scene = new Scene(grid, 800, 400);

                final NumberAxis xLAxis = new NumberAxis();
                final NumberAxis yLAxis = new NumberAxis();
                LineChart lineChart
                        = new LineChart<>(xLAxis, yLAxis);
                XYChart.Series series1
                        = new XYChart.Series<>();
                series1.setName("Partida 1");
                series1.getData().add(new XYChart.Data(2, 23));
                series1.getData().add(new XYChart.Data(4, 14));
                series1.getData().add(new XYChart.Data(6, 15));
                series1.getData().add(new XYChart.Data(8, 24));
                series1.getData().add(new XYChart.Data(10, 34));
                series1.getData().add(new XYChart.Data(12, 36));
                series1.getData().add(new XYChart.Data(14, 22));
                series1.getData().add(new XYChart.Data(16, 45));
                series1.getData().add(new XYChart.Data(18, 43));
                series1.getData().add(new XYChart.Data(20, 17));

                lineChart.getData().addAll(series1);
                grid.add(lineChart, 1, 0);

                NumberAxis lineYAxis
                        = new NumberAxis(0, 10, 1);
                lineYAxis.setLabel("Quantidade");
                CategoryAxis lineXAxis = new CategoryAxis();
                lineXAxis.setLabel("Características");
                BarChart barChart
                        = new BarChart<>(lineXAxis, lineYAxis);
                XYChart.Series bar1
                        = new XYChart.Series<>();
                bar1.setName("Partida 1");
                int valores[] = ControladorHistorico.criarGrafico();
                bar1.getData().add(getData(valores[0], "Pouco"));
                bar1.getData().add(getData(valores[1], "Médio"));
                bar1.getData().add(getData(valores[2], "Bom"));
                bar1.getData().add(getData(valores[3], "Ótimo"));

                //XYChart.Series bar2 = new XYChart.Series<>();
                //bar2.getData().add(getData(5,"Muito bom"));
                //bar2.getData().add(getData(2,"Ótimo"));
                //bar2.getData().add(getData(1,"Ruim"));
                //bar2.getData().add(getData(3,"Muito ruim"));
                barChart.getData().addAll(bar1);
                grid.setVgap(20);
                grid.setHgap(20);
                grid.add(barChart, 2, 0);
                fxPanel.setScene(scene);
            }
        });

    }

    private XYChart.Data getData(double x, double y) {
        XYChart.Data data = new XYChart.Data<>();
        data.setXValue(x);
        data.setYValue(y);
        return data;
    }

    private XYChart.Data getData(double x, String y) {
        XYChart.Data data = new XYChart.Data<>();
        data.setYValue(x);
        data.setXValue(y);
        return data;
    }

}
