/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.apooag.sef.fronteira;

import br.apooag.sef.controlador.ControladorJogo;
import br.apooag.sef.entidade.TipoAcao;

/**
 *
 * @author Gilmar
 */
public class TelaEscolherTipoAcao extends javax.swing.JFrame {

    /**
     * Creates new form TelaEscolherAcao
     */
    public TelaEscolherTipoAcao() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        labelArte = new javax.swing.JLabel();
        labelBrincadeira = new javax.swing.JLabel();
        labelTragedia = new javax.swing.JLabel();
        buttonCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Escolher Ação");
        setPreferredSize(new java.awt.Dimension(768, 510));
        setSize(new java.awt.Dimension(768, 510));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });

        jLabel1.setText("Escolher Ação");

        labelArte.setBackground(new java.awt.Color(153, 255, 153));
        labelArte.setText("Arte");
        labelArte.setEnabled(false);
        labelArte.setOpaque(true);
        labelArte.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                labelArteMouseClicked(evt);
            }
        });

        labelBrincadeira.setBackground(new java.awt.Color(153, 255, 153));
        labelBrincadeira.setText("Brincadeira");
        labelBrincadeira.setEnabled(false);
        labelBrincadeira.setOpaque(true);
        labelBrincadeira.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                labelBrincadeiraMouseClicked(evt);
            }
        });

        labelTragedia.setBackground(new java.awt.Color(153, 255, 153));
        labelTragedia.setText("Tragédia");
        labelTragedia.setOpaque(true);
        labelTragedia.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                labelTragediaMouseClicked(evt);
            }
        });

        buttonCancelar.setText("cancelar");
        buttonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addComponent(labelArte, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(labelBrincadeira, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addComponent(labelTragedia, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(77, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonCancelar)
                .addGap(65, 65, 65))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(buttonCancelar))
                .addGap(76, 76, 76)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelArte, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelTragedia, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelBrincadeira, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(114, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void labelArteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_labelArteMouseClicked
        //ControladorJogo.escolherTipoAcao(TipoAcao.ARTE);
    }//GEN-LAST:event_labelArteMouseClicked

    private void labelBrincadeiraMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_labelBrincadeiraMouseClicked
        //ControladorJogo.escolherTipoAcao(TipoAcao.BRINCADEIRA);
    }//GEN-LAST:event_labelBrincadeiraMouseClicked

    private void labelTragediaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_labelTragediaMouseClicked
        ControladorJogo.escolherTipoAcao(TipoAcao.TRAGEDIA);
    }//GEN-LAST:event_labelTragediaMouseClicked

    private void buttonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCancelarActionPerformed
        ControladorJogo.voltarParaInicio();
    }//GEN-LAST:event_buttonCancelarActionPerformed

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        TipoAcao tipoAcaoAtual = ControladorJogo.getTipoAcaoAtual();
        if(tipoAcaoAtual != null){
            if(tipoAcaoAtual.equals(TipoAcao.ARTE)){
                labelArte.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 0, 0), 5));
                
                labelBrincadeira.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 0, 0), 0));
                labelTragedia.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 0, 0), 0));
            }
            else if(tipoAcaoAtual.equals(TipoAcao.BRINCADEIRA)){
                labelBrincadeira.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 0, 0), 5));
                
                labelArte.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 0, 0), 0));
                labelTragedia.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 0, 0), 0));
            }else if(tipoAcaoAtual.equals(TipoAcao.TRAGEDIA)){
                labelTragedia.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 0, 0), 5));
                
                labelBrincadeira.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 0, 0), 0));
                labelArte.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 0, 0), 0));
            }
        }
    }//GEN-LAST:event_formWindowActivated

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaEscolherTipoAcao.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaEscolherTipoAcao.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaEscolherTipoAcao.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaEscolherTipoAcao.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaEscolherTipoAcao().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonCancelar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel labelArte;
    private javax.swing.JLabel labelBrincadeira;
    private javax.swing.JLabel labelTragedia;
    // End of variables declaration//GEN-END:variables
}
