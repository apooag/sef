/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.apooag.sef.fronteira;

import br.apooag.sef.controlador.ControladorPartida;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Gilmar
 */
public class Sensor implements Eyetracker{
    private final Random gerador = new Random();
    private Timer timer;

    class Loop extends TimerTask {
        int num = 0;
        @Override
        public void run() {
            int duracaoEspera = getTempoEspera();
            int duracaoContato = getTempoInteracao();
            
          try{
                Thread.sleep((duracaoEspera+duracaoContato)*1000);
                ControladorPartida.interacaoDetectada(duracaoContato);
           }catch(InterruptedException e){
                e.printStackTrace();
            }
        }
    }
    
    @Override
    public void ativar(int[] posicaoOlhos, int diametroOlhos) {
       System.out.println("Eyetracker ativado");
        timer = new Timer();
        timer.schedule(new Sensor.Loop(), 0, 1*1000);
    }

    public int getTempoInteracao(){ 
        return gerador.nextInt(12);
    }
    
    public int getTempoEspera(){ 
        return gerador.nextInt(61);
    }
    
    @Override
    public void desativar() {
        timer.cancel();
        System.out.println("Eyetracker desativado");  
    }
    
    
}
