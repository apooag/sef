/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.apooag.sef.entidade;

import java.util.Random;

/**
 *
 * @author Gilmar
 */
public class Acao implements Realizavel{
    private final String nome;
    private Intensidade intensidade;
    private final String[] recursos;
    private final TipoAcao tipo;
    Random gerador = new Random();

    public Acao(String nome, String[] variantes, TipoAcao tipo) {
        this.nome = nome;
        this.recursos = variantes;
        this.tipo = tipo;
    }
    
    
    @Override
    public String getRecurso() {
        return recursos[gerador.nextInt(2)]+"_"+intensidade.name()+".gif";
    }
    
    public void setIntensidade(Intensidade intensidade){
        this.intensidade = intensidade;
    }
    
    public TipoAcao getTipo(){
        return tipo;
    }
    
    public String getNome(){
        return nome;
    }
}