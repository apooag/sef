/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.apooag.sef.entidade;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Gilmar
 */
public class Interacao implements Serializable{
    private int duracaoContatoEfetivo;
    private Intensidade intensidade;
    private NivelHabilidade nivel;
    private Date dataInteracao;

    public Interacao(int duracaoContatoEfetivo, Intensidade intensidade, NivelHabilidade nivel, Date dataInteracao) {
        this.duracaoContatoEfetivo = duracaoContatoEfetivo;
        this.intensidade = intensidade;
        this.nivel = nivel;
        this.dataInteracao = dataInteracao;
    }
    
    public int getDuracaoContatoEfetivo() {
        return duracaoContatoEfetivo;
    }

    public void setDuracaoContatoEfetivo(int duracaoContatoEfetivo) {
        this.duracaoContatoEfetivo = duracaoContatoEfetivo;
    }

    public Intensidade isIntensidade() {
        return intensidade;
    }

    public void setIntensidade(Intensidade intensidade) {
        this.intensidade = intensidade;
    }

    public NivelHabilidade getNivel() {
        return nivel;
    }

    public void setNivel(NivelHabilidade nivel) {
        this.nivel = nivel;
    }

    public Date getDataInteracao() {
        return dataInteracao;
    }

    public void setDataInteracao(Date dataInteracao) {
        this.dataInteracao = dataInteracao;
    }

    @Override
    public String toString() {
        return "Interacao{ " + "duracaoContatoEfetivo=" + duracaoContatoEfetivo + ", intensidade=" + intensidade + ", nivel=" + nivel + ", dataInteracao=" + dataInteracao + " }";
    }
    
    
    
}
