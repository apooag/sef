/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.apooag.sef.entidade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Gilmar
 */
public class Partida implements Serializable{
    private final Personagem personagem;
    private final TipoAcao tipoAcao;
    private NivelHabilidade nivelHabilidade;
    private final List<Interacao> listaInteracoes;
    private EstadoPartida estado;
    private int quantidadeNivelOtimoConsecutivos;
    private boolean ganhou;
    private final Date dataInicio;

    public Partida(Personagem personagem, TipoAcao tipoAcao) {
        this.quantidadeNivelOtimoConsecutivos = 0;
        this.ganhou = false;
        this.dataInicio = new Date(2016); //ajeitar

        this.personagem = personagem;
        this.tipoAcao = tipoAcao;
        this.nivelHabilidade = NivelHabilidade.POUCO;
        this.listaInteracoes = new ArrayList<>();
        this.estado = EstadoPartida.AGUARDANDO;
    }

    public Personagem getPersonagem() {
        return personagem;
    }

    public TipoAcao getTipoAcao() {
        return tipoAcao;
    }

    public NivelHabilidade getNivelHabilidade() {
        return nivelHabilidade;
    }

    public void setNivelHabilidade(NivelHabilidade nivel) {
        if (this.nivelHabilidade.equals(NivelHabilidade.OTIMO) && nivel.equals(NivelHabilidade.OTIMO)) {
            this.quantidadeNivelOtimoConsecutivos++;
            if (this.quantidadeNivelOtimoConsecutivos >= 5) {
                this.ganhou = true;
            }
        } else {
            this.quantidadeNivelOtimoConsecutivos = 1;
        }
        this.nivelHabilidade = nivel;
    }

    public Intensidade addInteracao(int duracaoContato) {
        atualizarNivelHabilidade(duracaoContato);
        
        Intensidade intensidade = Intensidade.FRACO;
        if (duracaoContato >= nivelHabilidade.getDuracaoContatoIdeal()) {//interacão com muita intensidade
            System.out.println("INTENSIDADE ALTA  interacao: " + duracaoContato + "s  NH:" + nivelHabilidade.name() + " contatoIdeal:" + nivelHabilidade.getDuracaoContatoIdeal());
            intensidade = Intensidade.FORTE;
        } else {
            System.out.println("INTENSIDADE BAIXA  interacao: " + duracaoContato + "s  NH:" + nivelHabilidade.name() + " contatoIdeal:" + nivelHabilidade.getDuracaoContatoIdeal());
        }
        
        estado = EstadoPartida.REALIZANDO_ACAO;

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Date hora = Calendar.getInstance().getTime(); // Ou qualquer outra forma que tem
        String dataFormatada = sdf.format(hora);
        Interacao i = new Interacao(duracaoContato, intensidade, nivelHabilidade, hora);
        listaInteracoes.add(i);
        
        return intensidade;

    }

    private void atualizarNivelHabilidade(int duracaoContato) {
        int nivel = duracaoContato * 10 / nivelHabilidade.getDuracaoContatoIdeal();
        if (nivel <= NivelHabilidade.POUCO.getIntervaloValores()[1]) {
            //POUCO
            setNivelHabilidade(NivelHabilidade.POUCO);
        } else if (nivel > NivelHabilidade.MEDIO.getIntervaloValores()[0] && nivel <= NivelHabilidade.MEDIO.getIntervaloValores()[1]) {
            //MEDIO
            setNivelHabilidade(NivelHabilidade.MEDIO);
        } else if (nivel > NivelHabilidade.BOM.getIntervaloValores()[0] && nivel <= NivelHabilidade.BOM.getIntervaloValores()[1]) {
            //BOM
            setNivelHabilidade(NivelHabilidade.BOM);
        } else if (nivel > NivelHabilidade.OTIMO.getIntervaloValores()[0]) {
            //OTIMO
            setNivelHabilidade(NivelHabilidade.OTIMO);
        }
    }

    public boolean isGanhou() {
        return ganhou;
    }

    public void setGanhou(boolean ganhou) {
        this.ganhou = ganhou;
    }

    public EstadoPartida getEstado() {
        return estado;
    }

    public void setEstado(EstadoPartida estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        String string = "Partida{ " + "personagem=" + personagem + ", tipoAcao=" + tipoAcao + ", nivelHabilidade=" + nivelHabilidade + ", ganhou=" + ganhou + "\n";
        for(Interacao interacao: listaInteracoes){
            string+= "     "+interacao+"\n";
        }
        string+="}";
        return string;
    }

    
}
