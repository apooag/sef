/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.apooag.sef.entidade;

/**
 *
 * @author Gilmar
 */
public enum NivelHabilidade {
    POUCO(2, -1, 3 ),
    MEDIO(5, 3, 6),
    BOM(8, 6, 10),
    OTIMO(11, 10, 100);
    
    public int duracaoContatoIdeal;
    public int[] intervaloValores;
    
    NivelHabilidade(int duracao, int intervaloInicio, int intervaloFim){
        this.duracaoContatoIdeal = duracao;
        this.intervaloValores = new int[2];
        this.intervaloValores[0] = intervaloInicio;
        this.intervaloValores[1] = intervaloFim;
    }

    public int getDuracaoContatoIdeal() {
        return duracaoContatoIdeal;
    }

    public int[] getIntervaloValores() {
        return intervaloValores;
    }
}
