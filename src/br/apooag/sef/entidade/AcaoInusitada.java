/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.apooag.sef.entidade;


/**
 *
 * @author Gilmar
 */
public class AcaoInusitada implements Realizavel{
    private final String nome;
    private final String recurso;
    private final TipoAcao tipo;

    public AcaoInusitada(String nome, TipoAcao tipo, String recurso){
        this.nome = nome;
        this.tipo = TipoAcao.TRAGEDIA;
        this.recurso = recurso;
    }
    
    @Override
    public String getRecurso() {
        return recurso+".gif";
    }

    public TipoAcao getTipo() {
        return tipo;
    }
    
    public String getNome() {
        return nome;
    }
}
