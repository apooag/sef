/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.apooag.sef.entidade;

/**
 *
 * @author Gilmar
 */
public enum EstadoPartida {
    AGUARDANDO,
    REALIZANDO_ACAO,
    CHAMANDO_ATENCAO,
    REALIZANDO_ACAO_INUSITADA;
}
