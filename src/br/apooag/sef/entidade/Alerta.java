/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.apooag.sef.entidade;

/**
 *
 * @author Gilmar
 */
public class Alerta implements Realizavel{
    private final String nome;
    private final String recurso;

    public Alerta(String nome, String recurso) {
        this.nome = nome;
        this.recurso = recurso;
    }

    @Override
    public String getRecurso() {
        return recurso+".gif";
    }

    public String getNome() {
        return nome;
    }
    
}
