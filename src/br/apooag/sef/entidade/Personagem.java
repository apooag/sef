/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.apooag.sef.entidade;

/**
 *
 * @author Gilmar
 */
public enum Personagem {
    GATO(0, 0 ,0),
    CACHORRO(0, 0,0),
    MENINO(0, 0, 0),
    MENINA(0, 0, 0);
    
    public int[] posicaoOlhos;
    public int diametroOlhos;

    Personagem(int posicaoXOlhos, int posicaoYOlhos, int diametroOlhos){
        this.posicaoOlhos = new int[2];
        this.posicaoOlhos[0] = posicaoXOlhos;
        this.posicaoOlhos[1] = posicaoYOlhos;
        this.diametroOlhos = diametroOlhos;
    }
    
    public int[] getPosicaoOlhos(){
        return posicaoOlhos;
    }
    public int getDiametroOlhos(){
        return diametroOlhos;
    }
    
}
