/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.apooag.sef.entidade;

/**
 *
 * @author Gilmar
 */
public class Aguardando implements Realizavel{
    private final String recurso;
    
    public Aguardando(String recurso){
        this.recurso = recurso;
    }
    @Override
    public String getRecurso() {
        return recurso+".gif";
    }
    
}
